<?php
/**
 * The template for displaying the footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>

<footer class="footer">
    <div class="footer-widgets">
        <div class="container">        
            <?php if ( is_active_sidebar( 'coopercica-sidebar-footer-1' ) ) : ?>
                <div class="widget-column"> 
                    <?php dynamic_sidebar( 'coopercica-sidebar-footer-1' ); ?> 
                </div>
            <?php endif; ?>
            <?php if ( is_active_sidebar( 'coopercica-sidebar-footer-2' ) ) : ?>
                <div class="widget-column"> 
                    <?php dynamic_sidebar( 'coopercica-sidebar-footer-2' ); ?> 
                </div>
            <?php endif; ?>
            <?php if ( is_active_sidebar( 'coopercica-sidebar-footer-3' ) ) : ?>
                <div class="widget-column"> 
                    <?php dynamic_sidebar( 'coopercica-sidebar-footer-3' ); ?> 
                </div>
            <?php endif; ?>
            <?php if ( is_active_sidebar( 'coopercica-sidebar-footer-4' ) ) : ?>
                <div class="widget-column"> 
                    <?php dynamic_sidebar( 'coopercica-sidebar-footer-4' ); ?> 
                </div>
            <?php endif; ?>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.footer-widgets -->

    <div class="footer-copyright">
        <div class="container px-3">
            <span class="text-center">&copy;&nbsp;<?php echo wp_date('Y'); ?>&nbsp;<strong><?php echo bloginfo('title'); ?></strong>&nbsp;<?php _e('All rights reserved.', 'coopercica') ?>
            &nbsp;<?php _e( 'Design by', 'coopercica'); ?> <a href="https://novahorda.com/" target="_blank" rel="noopener">NHRD</a></span>
            <?php
            wp_nav_menu( array(
                'theme_location' => 'footer_menu',
                'depth' => 1,
                'container_class' => 'footer-menu-wrap',
                'menu_class' => 'footer-menu',
            ) );
            ?>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.footer-copyright -->
</footer>   

<?php wp_footer(); ?>

</body>

</html>