<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */
get_header();
?>

<main class="container my-5">
    <?php
    // If has thumbnail and its can be displayed
    if ( has_post_thumbnail() && get_the_post_thumbnail() ) {
        the_post_thumbnail( 'medium', array(
            'class' => 'rounded-circle img-fluid',
            'title' => esc_html( get_the_title() ), // Use post title as img title
            'alt' => esc_html( get_the_title() ), // Use post title as alt
        ) );
    } else {
        echo sprintf( '<img class="wp-post-image rounded-circle img-fluid" src="%s" alt="%s">', get_template_directory_uri() . '/assets/images/default-thumbnail-300.jpg', __( 'Default Image', 'coopercica' ) );
    }
    ?>

    <?php get_sidebar(); ?>
    
    <?php
    while ( have_posts() ) {
        the_post();

        get_template_part( 'partials/content/content', 'single' );

        /**
         * 
         * Build the links for previous and next post navigation. 
         * 
         **/
        $next_post_id = get_next_post()->ID;
        $next_post_thumbnail = get_the_post_thumbnail( 
            $next_post_id,
            'thumbnail_post_nav',
            array(
                'class' => 'rounded-circle',
                'title' => esc_html( get_the_title( $next_post_id ) ), // Use post title as img title
                'alt' => esc_html( get_the_title( $next_post_id ) ), // Use post title as alt
            )
        );
        // If has no thumbnail use a default one
        if (!$next_post_thumbnail) {
            $next_post_thumbnail = sprintf( '<img class="rounded-circle" src="%s" alt="%s">', get_template_directory_uri() . '/assets/images/default-thumbnail-100.jpg', __( 'Default Image', 'coopercica' ) );
        }

        $previous_post_id = get_previous_post()->ID;
        $previous_post_thumbnail = get_the_post_thumbnail( 
            $previous_post_id,
            'thumbnail_post_nav',
            array(
                'class' => 'rounded-circle',
                'title' => esc_html( get_the_title( $previous_post_id ) ), // Use post title as img title
                'alt' => esc_html( get_the_title( $previous_post_id ) ), // Use post title as alt
            )
        );
        // If has no thumbnail use a default one
        if (!$previous_post_thumbnail) {
            $previous_post_thumbnail = sprintf( '<img class="rounded-circle" src="%s" alt="%s">', get_template_directory_uri() . '/assets/images/default-thumbnail-100.jpg', __( 'Default Image', 'coopercica' ) );
        }
        
        the_post_navigation( array(
            'next_text' => '<div><span class="meta-nav" aria-hidden="true">' . __( 'Next', 'coopercica' ) . '</span> ' .
                '<span class="screen-reader-text">' . __( 'Next post:', 'coopercica' ) . '</span> <br/>' .
                '<span class="post-title">%title</span></div><div class="ml-3">' . $next_post_thumbnail . '</div>',
            'prev_text' => '<div class="mr-3">' . $previous_post_thumbnail . '</div><div><span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'coopercica' ) . '</span> ' .
                '<span class="screen-reader-text">' . __( 'Previous post:', 'coopercica' ) . '</span> <br/>' .
                '<span class="post-title">%title</span></div>',
        ) );

        // Comments
        if (comments_open() || get_comments_number()) comments_template();
    }
    ?>

</main>
<!--/.container-->

<?php
get_footer();