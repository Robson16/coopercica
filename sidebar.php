<?php // Sidebar Blog ?>

<aside class="sidebar">
    <?php if ( is_active_sidebar( 'coopercica-sidebar-blog' ) ) dynamic_sidebar( 'coopercica-sidebar-blog' ); ?>
</aside>