<?php

/**
 * 
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

// 
function coopercica_scripts() {
    // CSS
    wp_enqueue_style( 'bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css', null, '4.5.0', 'all' );
    wp_enqueue_style( 'coopercica-webfonts-styles', get_template_directory_uri() . '/assets/css/shared/webfont.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'coopercica-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'coopercica-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get( 'Version' ) );

    // Js
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//code.jquery.com/jquery-3.4.1.min.js', array(), '3.4.1', true );
    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'comment-reply' );
    wp_enqueue_script( 'popper', '//cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', array( 'jquery' ), '1.16.0', true );
    wp_enqueue_script( 'bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js', array( 'jquery', 'popper' ), '4.5.0', true );
}

add_action('wp_enqueue_scripts', 'coopercica_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function coopercica_gutenberg_scripts() {
    wp_enqueue_style( 'coopercica-webfonts-styles', get_template_directory_uri() . '/assets/css/shared/webfont.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_script( 'coopercica-editor-script', get_stylesheet_directory_uri() . '/assets/js/admin/editor.js', array( 'wp-blocks', 'wp-dom' ), '1.0', true );
}

add_action('enqueue_block_editor_assets', 'coopercica_gutenberg_scripts');

/** 
 * Set theme defaults and register support for various WordPress features.
 */
function coopercica_setup() {
    // Enabling translation support
    $textdomain = 'coopercica';
    load_theme_textdomain( $textdomain, get_stylesheet_directory() . '/languages/' );
    load_theme_textdomain( $textdomain, get_template_directory() . '/languages/' );

    // Customizable logo
    add_theme_support( 'custom-logo', array(
        'height'      => 180,
        'width'       => 250,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    ) );

    // Custom Header
    add_theme_support( 'custom-header', array(
        'default-image'      => get_template_directory_uri() . '/assets/images/default-header.jpg',
        'default-text-color' => 'fff',
        'width'              => 1920,
        'height'             => 460,
        'flex-width'         => true,
        'flex-height'        => true,
    ) );

    // Menu registration
    register_nav_menus(array(
        'top_menu' => __( 'Top Menu', 'coopercica' ),
        'main_menu' => __( 'Main Menu', 'coopercica' ),
        'footer_menu' => __( 'Footer Menu', 'coopercica' ),
    ));

    // Custom image sizes
    // OBS: Overwrite the medium size, still using the values entered in the panel, forcing it to crop the image
    add_image_size( 'medium', get_option( 'medium_size_w' ), get_option( 'medium_size_h' ), true );
    add_image_size( 'thumbnail_post_nav', 100, 100, true );

    // Let WordPress manage the document title.
    add_theme_support( 'title-tag' );
    
    // Enable support for featured image on posts and pages.		 	
    add_theme_support( 'post-thumbnails' );

    // Load custom styles in the editor.
    add_theme_support( 'editor-styles' );
    add_editor_style( get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css' );

    // Enable support for embedded media for full weight
    add_theme_support( 'responsive-embeds' );

    // Enables wide and full dimensions
    add_theme_support( 'align-wide' );

    // Standard style for each block.
    add_theme_support( 'wp-block-styles' );
    
    // Disable custom colors
    add_theme_support( 'disable-custom-colors' );

    // Creates the specific color palette
    add_theme_support('editor-color-palette', array(
        array(
            'name'  => __('White', 'coopercica'),
            'slug'  => 'white',
            'color'    => '#ffffff',
        ),
        array(
            'name'  => __('Black', 'coopercica'),
            'slug'  => 'black',
            'color'    => '#000000',
        ),
        array(
            'name'  => __('Pear', 'coopercica'),
            'slug'  => 'pear',
            'color'    => '#d7df21',
        ),
        array(
            'name'  => __('Dark Pastel Green', 'coopercica'),
            'slug'  => 'dark-pastel-green',
            'color'    => '#58ba47',
        ),
        array(
            'name'  => __('Forest Green Traditional', 'coopercica'),
            'slug'  => 'forest-green-traditional',
            'color'    => '#1a4723',
        ),
        array(
            'name'  => __('Cinnabar', 'coopercica'),
            'slug'  => 'cinnabar',
            'color'    => '#ef3e37',
        ),
    ));

    // Disable custom gradients
    add_theme_support( 'disable-custom-gradients' );
}

add_action( 'after_setup_theme', 'coopercica_setup' );

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function coopercica_sidebars() {
    // Args used in all calls register_sidebar().
    $shared_args = array(
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
        'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
        'after_widget' => '</div></div>',
    );
    
    // Footer #1
    register_sidebar( array_merge( $shared_args, array(
        'name' => __( 'Footer #1', 'coopercica' ),
        'id' => 'coopercica-sidebar-footer-1',
        'description' => __( 'The widgets in this area will be displayed in the first column in Footer.', 'coopercica' ),
    ) ) );

    // Footer #2
    register_sidebar( array_merge( $shared_args, array(
        'name' => __( 'Footer #2', 'coopercica' ),
        'id' => 'coopercica-sidebar-footer-2',
        'description' => __( 'The widgets in this area will be displayed in the second column in Footer.', 'coopercica' ),
    ) ) );

    // Footer #3
    register_sidebar( array_merge( $shared_args, array(
        'name' => __( 'Footer #3', 'coopercica' ),
        'id' => 'coopercica-sidebar-footer-3',
        'description' => __( 'The widgets in this area will be displayed in the third column in Footer.', 'coopercica' ),
    ) ) );

    // Footer #4
    register_sidebar( array_merge( $shared_args, array(
        'name' => __( 'Footer #4', 'coopercica' ),
        'id' => 'coopercica-sidebar-footer-4',
        'description' => __( 'The widgets in this area will be displayed in the fourth column in Footer.', 'coopercica' ),
    ) ) );

    // Barra Lateral Blog #1
    register_sidebar( array_merge( $shared_args, array (
        'name' => __( 'Sidebar Blog', 'coopercica' ),
        'id' => 'coopercica-sidebar-blog',
        'description' => __( 'The widgets in this area will be displayed in the blog sidebar.', 'coopercica' ),
    ) ) );
}

add_action( 'widgets_init', 'coopercica_sidebars' );

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 *  Kirki Framework Config
 */
require_once get_template_directory() . '/includes/kirki/kirki-config.php';


/**
 *  Theme Custom Shortcodes
 */
require_once get_template_directory() . '/includes/custom-shortcodes.php';