<?php

// Theme Custom Shortcodes

/**
 * Add Carroussel Slide Tempalte-Part/Partials to a Shortcode
 */
function carousel_slide_shortcode( $atts ) {
    ob_start();
    get_template_part( 'partials/carousel/carousel', 'slide' );
    return ob_get_clean();
}
add_shortcode( 'carousel-slide', 'carousel_slide_shortcode' );

/**
 * Add Post List to a Shortcode
 */
function recent_posts_shortcode( $atts = array() ) {
    ob_start();
    
    // QTD of post to show
    $qtd = 3;
    if ( is_array( $atts ) && array_key_exists( "qtd", $atts ) ) {
        if ( (int)$atts["qtd"] > 0 ) {
            $qtd = $atts["qtd"];
        }     
    }

    $recentPosts = new WP_Query( array(
        'post_type' => 'post',
        'posts_per_page' => $qtd,
        'update_post_meta_cache' => false,
        'update_post_term_cache' => false,
    ) );

    echo "<div class='recent-posts'>";
        echo sprintf( '<div class="recent-posts-header"><h3 class="recent-posts-title">%s</h3></div>', __( 'Blog', 'coopercica' ) );
        echo '<div class="recent-posts-list">';
        while ( $recentPosts->have_posts() ) {
            $recentPosts->the_post();
            get_template_part( 'partials/content/content', 'excerpt' );
        }
        echo "</div>";
    echo "</div>";
    
    wp_reset_postdata();

    return ob_get_clean();
}
add_shortcode( 'recent-posts', 'recent_posts_shortcode' );

/**
 * Flyers with programmable display dates
 */
function flyers_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'images' => '',
			'start-date' => '01/01/1900',
			'end-date' => '31/12/1900',
		),
		$atts,
		'flyers'
    );
    
    // Format the dates
    $today = DateTime::createFromFormat ( 'd/m/Y', date( 'd/m/Y' ) );
	$start = DateTime::createFromFormat ( 'd/m/Y', $atts['start-date'] );
    $end = DateTime::createFromFormat ( 'd/m/Y', $atts['end-date'] );
    
    // Generates image tags
    $imagesArray = explode( ",", $atts['images'] );
    $imagesTags = '';
    foreach ( $imagesArray as $image ) {
        $imagesTags .= '<img class="d-block img-fluid mx-auto mb-3" src="' . esc_url( $image ) . '" alt="' . __( 'Flyer', 'coopercica' ) . '">';
    }
    
    // Determines whether it is between valid dates
    if( $start <= $today && $end >= $today ) {
		return
            '<div class="flyers">
                <p class="h4 text-center mb-3">
                    <strong>' . sprintf( __( 'Valid offers from %s to %s', 'coopercica' ), $atts['start-date'], $atts['end-date'] ) .'</strong>
                </p>'
                . $imagesTags . 
            '</div><!--END flyers-->';
    }

    // Returns empty if not between valid dates
    return;
}
add_shortcode( 'flyers', 'flyers_shortcode' );