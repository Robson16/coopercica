<?php

function coopercica_kirki() {

    if ( class_exists( 'Kirki' ) ) {

        Kirki::add_config( 'coopercica_kirki_config', array (
            'capability' => 'edit_theme_options',
            'option_type' => 'theme_mod',
        ) );       
        
        require_once get_template_directory() . '/includes/kirki/kirki-control-carousel.php';
    }

}

add_action( 'customize_register', 'coopercica_kirki' );