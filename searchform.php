<?php

/**
 * Template for displaying search forms
 * 
 */
?>

<form role="search" method="get" class="search-form form-inline w-100" action="<?php echo esc_url( home_url( '/' ) ); ?>">

    <label class="search-label d-none" for="s"><?php _e('Search', 'coopercica'); ?></label>

    <div class="input-group flex-grow-1">
        <input 
            type="search" 
            id="s" 
            name="s" 
            class="search-field form-control flex-grow-1" 
            placeholder="<?php _e('Search for', 'coopercica') ?>" 
            value="<?php echo get_search_query(); ?>" 
            aria-label="<?php _e('Search for', 'coopercica'); ?>" 
        />

        <div class="input-group-append">
            <button class="search-submit" type="submit"><?php _e('Search!', 'coopercica'); ?></button>
        </div>
    </div>
</form>