<?php
$slides = get_theme_mod( 'setting_carousel' );
$is_mobile = wp_is_mobile();

if ($slides) : 
?>
    
<div id="carousel-slide" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <?php for ($x = 0; $x < sizeof($slides); $x++) : ?>
            <div class="carousel-item <?php if ($x === 0) echo 'active'; ?>">
                <img 
                    class="d-block w-100" 
                    src="<?php echo wp_get_attachment_image_src(($is_mobile) ? $slides[$x]['slide_mobile'] : $slides[$x]['slide_desktop'], 'full')[0]; ?>" 
                    alt="<?php echo get_post_meta(($is_mobile) ? $slides[$x]['slide_mobile'] : $slides[$x]['slide_desktop'], '_wp_attachment_image_alt', TRUE); ?>"
                >
            </div>
        <?php endfor; ?>
    </div>
    <!-- /.carousel-inner -->

    <a class="carousel-control-prev" href="#carousel-slide" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only"><?php _e( 'Previous', 'coopercica'); ?></span>
    </a>
    <a class="carousel-control-next" href="#carousel-slide" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only"><?php _e( 'Next', 'coopercica'); ?></span>
    </a>
</div>
<!-- /#carousel-slide -->
    
<?php endif; ?>