wp.domReady(() => {
    wp.blocks.registerBlockStyle('core/heading', {
        name: 'vertical-text',
        label: 'Texto Vertical',
    }); 
    wp.blocks.registerBlockStyle('core/cover', {
        name: 'cover-overflow-top',
        label: 'Transbordar Topo',
    });
    wp.blocks.registerBlockStyle('core/cover', {
        name: 'min-height-none',
        label: 'Sem altura minima',
    });
    wp.blocks.registerBlockStyle('core/columns', {
        name: 'mobile-two-columns',
        label: 'Duas Colunas em Celular',
    });
    wp.blocks.registerBlockStyle('core/column', {
        name: 'cinnabar-column',
        label: 'Coluna Cinábrio',
    });
    wp.blocks.registerBlockStyle('core/column', {
        name: 'green-column',
        label: 'Coluna Verde',
    });    
});