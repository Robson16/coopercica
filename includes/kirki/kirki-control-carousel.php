<?php

Kirki::add_section( 'section_carousel', array(
    'title' => esc_html__( 'Carrossel', 'coopercica' ),
    'priority' => 160,
));

Kirki::add_field( 'coopercica_kirki_config', [
    'type' => 'repeater',
    'label' => esc_html__( 'Slides', 'coopercica' ),
    'description' => esc_html__( 'Add a Carrossel Slide with these images using this shortcode: ', 'coopercica' ) . '<b>[carousel-slide]</b>',
    'section' => 'section_carousel',
    'priority' => 10,
    'row_label' => [
        'type' => 'field',
        'value' => esc_html__( 'Slide', 'coopercica' ),
        'field' => 'link_text',
    ],
    'button_label' => esc_html__( 'Add new', 'coopercica' ),
    'settings' => 'setting_carousel',
    'fields' => [
        'slide_desktop' => [
            'type' => 'image',
            'label' => esc_html__( 'Slide', 'coopercica' ),
            'choices' => [
                'save_as' => 'id',
            ],
        ],
        'slide_mobile' => [
            'type' => 'image',
            'label' => esc_html__( 'Slide Mobile', 'coopercica' ),
            'choices' => [
                'save_as' => 'id',
            ],
        ],
    ]
]);
