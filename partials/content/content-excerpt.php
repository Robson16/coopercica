<?php

/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'd-flex align-items-center flex-column flex-md-row mb-4' ); ?>>
	<div>
		<a class="post-thumbnail" href="<?php echo esc_url( get_permalink() ); ?>">
			<?php // If has thumbnail and its can be displayed ?>
			<?php if( has_post_thumbnail() && get_the_post_thumbnail() ): ?>
				<figure title="<?php the_title_attribute(); ?>">			
					<?php
					the_post_thumbnail( 'thumbnail', array(
						'class' => 'wp-post-image rounded-circle img-fluid mb-3 mb-lg-0 mr-lg-3',
						'title' => esc_html( get_the_title() ),
						'alt' => esc_html( get_the_title() ),
					) );
					?>
				</figure>
			<?php else: ?>
				<img 
					class="wp-post-image rounded-circle img-fluid mb-3 mb-lg-0 mr-lg-3" 
					src="<?php echo get_template_directory_uri() . '/assets/images/default-thumbnail-150.jpg' ?>" 
					alt="<?php _e( 'Default Image', 'coopercica' ) ?>"
				>
			<?php endif; ?>
		</a>
	</div>

	<div>
		<header class="entry-header">
			<?php
			if ( is_sticky() && is_home() && !is_paged() ) {
				printf( '<span class="sticky-post">%s</span>', _x( 'Post', 'Featured', 'coopercica' ) );
			}
			the_title( sprintf( '<h4 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' );
			?>
			<p class="post-infos mb-1">
				<i class="dashicons dashicons-calendar-alt"></i>
				<span><?php echo get_the_date(); ?>&nbsp;</span>

				<i class="dashicons dashicons-category"></i>
				<span><?php the_category(', '); ?>&nbsp;</span>

				<?php if ( has_tag() ) : ?>
					<i class="dashicons dashicons-tag"></i>
					<span><?php the_tags('', ', ', ''); ?>&nbsp;</span>
				<?php endif; ?>

				<?php if ( get_comments_number() ) : ?>
					<i class="dashicons dashicons-admin-comments"></i>
					<span><?php echo get_comments_number(); ?></span>
				<?php endif; ?>
			</p>
		</header>
		<!-- /.entry-header -->

		<div class="entry-content">
			<p><?php echo wp_trim_words( get_the_content(), 35 ); ?></p>
		</div>
		<!-- /.entry-content -->
	</div>		
</article><!-- #post-<?php the_ID(); ?> -->