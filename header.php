<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <header id="header">
        <div id="topbar" class="topbar">
            <div class="container">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'top_menu',
                    'depth' => 1,
                    'container_class' => 'topbar-menu-wrap',
                    'menu_class' => 'topbar-menu',
                ) );
                ?>
            </div>
        </div>
        <nav id="navbar" class="navbar navbar-expand-lg">
            <div class="container">
                <?php if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) : ?>
                    <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                        <img 
                            class="img-fluid" 
                            src="<?php echo wp_get_attachment_image_src(get_theme_mod('custom_logo'), 'full')[0]; ?>" 
                            alt="<?php bloginfo('name'); ?>" 
                            title="<?php bloginfo('name'); ?>"
                        >
                    </a>
                <?php else: ?>
                    <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                        <h1><?php echo get_bloginfo( 'title' ); ?></h1>
                    </a>
                <?php endif; ?>

                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbar-nav" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <?php
                wp_nav_menu( array(
                    'theme_location' => 'main_menu',
                    'depth' => 2,
                    'container' => 'div',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id' => 'navbar-nav',
                    'menu_class' => 'navbar-nav',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker()
                ) );
                ?>
            </div>
        </nav>

        <?php if( !is_page() ): ?>
            <div class="header-inner" style="background-image: url(<?php echo get_header_image(); ?>); color: #FFF">
                <div class="container">    
                    <?php 
                    if ( is_search() ) echo sprintf( '<h1 class="header-title">%s <span>%s</span></h1>', __( 'Search results for:', 'coopercica'), get_search_query() );
                    if ( is_archive() ) echo sprintf( '<h1 class="header-title">%s</h1>', get_the_archive_title() );
                    if ( is_single() ) echo sprintf( '<h4 class="header-title">%s</h4>', __( 'Blog', 'coopercica' ) );
                    if ( is_home() ) echo sprintf( '<h1 class="header-title">%s</h1>', __( 'Blog', 'coopercica' ) );
                    ?>
                </div>
            </div>
        <?php endif; ?>
    </header>